OrteInBulgarien
=============

Current build: 1.0.0.0

Introduction
-------------
A guide of interesting places in Bulgaria (German)


Revisions
=========

Version 1.0
-----------
 + Supports: Initial version
 
Known issues
------------
 + For some reason adding "Back" button to the action bar does not work correct. When clicked an "undefined" error occurs.
 

License notes
=============

Graphic assets
--------------
The graphic assets are not free and therefore it is not allowed to be 
downloaded, copied, redistributed or used in any other way.

Notes
=====

It is possible to use a local webserver to update the application without recompilation during development. The following changes to config.xml are necessary to enable over the network development:

In config.xml:

	<!-- Development location - BEGIN - comment out for production-->

	<content src="http://<server name/ip>/PlacesBulgaria/source/index.html" />
	<access uri="http://<server name/ip>" subdomains="true" />
	<rim:cache disableAllCache="true" />

	<!-- Development location - END -->
	
This replaces the default <content src="index.html" /> with a network address.

The parameter <rim:cache disableAllCache="true" /> disables caching but it is not available on BB10.

Example:

	<!-- Development location - BEGIN - comment out for production-->

	<content src="http://192.168.0.103/PlacesBulgaria/source/index.html" />
	<access uri="http://192.168.0.103" subdomains="true" />
	<rim:cache disableAllCache="true" />

	<!-- Development location - END -->

A suitable web server is hfs: http://www.rejetto.com/hfs/	
